import basicauth
from flask import Blueprint, request, jsonify, render_template
from werkzeug.utils import redirect
from auth.models import User
from auth.models import mytable
import dijkstra
from werkzeug.security import check_password_hash

main = Blueprint('main', __name__)


@main.route('/maps', methods=['POST','GET'])
def insert():
    if request.method == 'POST':
        token = request.headers.get('Authorization')
        user, senha = basicauth.decode(token)
    else:
        user = request.args.get('user')
        senha = request.args.get('senha')
    user = User.query.filter_by(name=user).first()

    if not user or not check_password_hash(user.password, senha) or not user.status == 'A':
        return render_template('404.html'), 404
    else:
          post_data = request.get_json()
          if not mytable.find_one({'title': post_data['title']}):

                mytable.insert(post_data)
                arquivo = open("malha.log", "a")
                arquivo.write('\n'+str(user) +'==>' + str(post_data) )
                arquivo.close()
          else:
              mytable.update_one({'title': post_data['title']},{"$set":post_data},upsert=True)
          return redirect('/maps/all?user=admin&senha=admin')

@main.route('/maps/shortest', methods=['POST','GET'])
#@login_required
def getCollection():
  if request.method == 'POST':
        token = request.headers.get('Authorization')
        print(token)

        user, senha = basicauth.decode(token)
        print(user, senha)
  else:
        user = request.args.get('user')
        senha = request.args.get('senha')

  user = User.query.filter_by(name=user).first()

  #print(user,senha)
  if not user or not check_password_hash(user.password, senha) or not user.status=='A':
      return render_template('404.html'), 404
  else:
    mapName = request.args.get('map')
    print(user, senha, mapName)
    origin = request.args.get('origin')  # Ponto de origem
    destiny = request.args.get('destiny')  # Ponto final (chegada)
    price = request.args.get('price')
    autonomy = request.args.get('autonomy')
    steps = []  # Lista que ira ser preenchida com todos os passos do caminho
    dictPaths = {}  # Dicionario com o valor de todos as rotas
    graph = dijkstra.Graph()  # Lib que faz o grafico de vertex para calcular o dijkstra
    vertex = []  # Lista de vertex
    try:
        map = mytable.find_one({'title': mapName})

    except:
        response = jsonify({'response': 'Application could not use the DB especifield'})
        response.status_code = 500
        return response

    # Fazendo a iteração para preencher uma lista
    # de vertex que serão usado na lib Graph()
    for vert in map['routes']:
        vertex.append(vert['origin'])
        vertex.append(vert['destiny'])

    # Iterando nos vertex setando para que nao se repitam
    for i in list(set(vertex)):
        graph.add_vertex(i)

    # Adicionando os vertexa lib Graph
    for vert in map['routes']:
        dictPaths[vert['origin'] + vert['destiny']] = vert['distance']
        graph.add_edge(vert['origin'], vert['destiny'], vert['distance'])

    # Verificando se os pontos existem
    if origin not in vertex:
        response = jsonify({'response': 'The parameter origin does not contain on map %s'
                                        % mapName})
        response.status_code = 400
        return response
    if destiny not in vertex:
        response = jsonify({'response': 'The parameter destiny does not contain on map %s'
                                        % mapName})
        response.status_code = 400
        return response

    dijkstra.dijkstra(graph, graph.get_vertex(origin), graph.get_vertex(destiny))
    target = graph.get_vertex(destiny)
    path = [target.get_id()]
    dijkstra.shortest(target, path)

    a = 0
    while a < (len(path[::-1]) - 1):
        steps.append(path[::-1][a] + path[::-1][a + 1])
        a = a + 1

    total = 0
    for i in steps:
        total = total + dictPaths[i]

    # Apenas formatando a lista para utf-8
    pathList = []
    for i in path[::-1]:
        pathList.append(i.encode('utf-8'))
    print(path,'path')

    # Fazendo o calculo do custo nessa rota de menor distancia
    cost = float(total) / float(autonomy) * float(price)

    response = []

    response.append({'Path': pathList})

    response.append({'Total KM': '%.2f' % total})
    response.append({'Cost': '%.2f' % cost})
    return jsonify(data=response)

@main.route('/maps/<mapa>', methods=['GET','POST'])
def search(mapa):
    data = []
   #print(mapa)
    if request.method == 'POST':
       token = request.headers.get('Authorization')
       print(token)

       user, senha = basicauth.decode(token)
       print(user,senha)
    else:
       user = request.args.get('user')
       senha = request.args.get('senha')

    user = User.query.filter_by(name=user).first()
    if not user or not check_password_hash(user.password, senha) or not user.status == 'A':
        return render_template('404.html'), 404
    else:
          if mapa != 'all':
            map = mytable.find_one({'title': mapa})
            data.append(map)
          else:
            for map in mytable.find():
                data.append(map)
          #print(data)
          return jsonify(str(data))


@main.route('/maps/delete', methods=['POST'])
def delete():
    if request.method == 'POST':
        token = request.headers.get('Authorization')
        user, senha = basicauth.decode(token)
    else:
        user = request.args.get('user')
        senha = request.args.get('senha')
    user = User.query.filter_by(name=user).first()

    if not user or not check_password_hash(user.password, senha) or not user.status == 'A':
        return render_template('404.html'), 404
    else:
          post_data = request.get_json()
          if mytable.find_one({'title': post_data['title']}):

                mytable.delete_one({'title': post_data['title']})
                arquivo = open("malha.log", "a")
                arquivo.write('\n'+str(user) +' deletou ==>' + str(post_data) )
                arquivo.close()
          else:
              return render_template('404.html'), 404

          return redirect('/maps/all?user=admin&senha=admin')






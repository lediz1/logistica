from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy

#importa as definiçoes em __ini__ do diretorio admin para o flask admin
from admin import admin
#Inicia o sqlalchemy que sera usado nos nossos models
db = SQLAlchemy()

from flask import Flask
def create_app():
    app = Flask(__name__,
                template_folder='./templates/',# Flask aponta qual a pagina de templates
                )
    app.config['SECRET_KEY'] = 'secret-key-goes-here'  #encripta e decripta a senha
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db.sqlite'  # DB utilizado
    app.config["FLASK_ADMIN_SWATCH"]= "united"  # inicialização do flask_admin
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True  # DB utilizado
    db.init_app(app) # cria o banco na aplicação flask
    login_manager = LoginManager()   # gerenciador do flask login
    login_manager.login_view = 'auth.login' # controla quem pode usar certas rotas com o @login_required em cima da rota
    login_manager.init_app(app)  # inica o flask login no app do flask
    from auth.models import User   # importa as classes com as tabelas em models
    admin.init_app(app)  #cria o flask_admin(configs estao em __init__ no diretorio admin)

    @login_manager.user_loader
    def load_user(user_id):
        # since the user_id is just the primary key of our user table, use it in the query for the user
        return User.query.get(int(user_id))
    # blueprint for auth routes in our app
    from auth.auth import auth as auth_blueprint
    app.register_blueprint(auth_blueprint)
    # blueprint for non-auth parts of app
    from main import main as main_blueprint
    app.register_blueprint(main_blueprint)
    return app


if __name__ == '__main__':
    from app import db, create_app
    db.create_all(app=create_app())
    appapi = create_app()                #cria o aplicativo acima
    from auth.models import User  # importa as tabelas criadas em auth.mmodels.py
    from app import db                   # importa o banco
    from flask_admin.contrib.sqla import ModelView  #flask_admin.contrib.sqla esta para sql
    admin.add_view(ModelView(User, db.session,category="tabelas"))   # cria a sessao de usuarios no admin
    appapi.run()





